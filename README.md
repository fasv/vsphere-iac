# Integration Terraform and Cloud-init in VMware Vsphere infrastructure
> In this project terraform creates VM from template, that must exist in VSphere. It may be a simple VM or template.

> For VM initialization complete successfully, the template VM must contain ***open-vm-tools*** and ***cloud-init*** version that contain VMware datasource(with cloud-init 21.4 works well). 
Without it, cloud-init itself and network interfaces ip addresses initialization will not be work