provider "vsphere" {
	user = "administrator@vsphere.local"
	password = "yourpassword"
	vsphere_server = "vsphere.yourdomain"
	allow_unverified_ssl = true
	}

data "vsphere_datacenter" "dc" {
 	name = "dc_name"
}

data "vsphere_compute_cluster" "cl" {
	name = "cluster_name"
	datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_distributed_virtual_switch" "dvs" {
	name = "dvSwitch"
	datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "dpg-test-network" {
	name = "dpg-test-network"
	datacenter_id = "${data.vsphere_datacenter.dc.id}"
	distributed_virtual_switch_uuid = "${data.vsphere_distributed_virtual_switch.dvs.id}"
}

data "vsphere_network" "dpg1" {
	name = "dpg1"
	datacenter_id = "${data.vsphere_datacenter.dc.id}"
	distributed_virtual_switch_uuid = "${data.vsphere_distributed_virtual_switch.dvs.id}"
}

data "vsphere_network" "dpg2" {
	name = "dpg2"
	datacenter_id = "${data.vsphere_datacenter.dc.id}"
	distributed_virtual_switch_uuid = "${data.vsphere_distributed_virtual_switch.dvs.id}"
}

data "vsphere_datastore" "storage" {
	name = "storage"
	datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "debian11" {
	name = "debian11"
	datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

output "dc_id" {
	description = "Datacenter ID"
	value	    = data.vsphere_datacenter.dc.id
}

output "datastore_id" {
	description = "datastore ID"
	value = data.vsphere_datastore.storage.id
}

output "dvs_id" {
	description = "DVS ID"
	value = data.vsphere_distributed_virtual_switch.dvs.id
}

output "dpg_id" {
	description = "DPG ID"
	value = data.vsphere_network.dpg-test-network.id
}

output "dpg_type" {
	description = "DPG TYPE"
	value = data.vsphere_network.dpg-test-network.type
}

output "vm_id" {
	description = "VM ID"
	value = data.vsphere_virtual_machine.debian11.id
}

output "cc_id" {
	description = "Cluster ID"
	value = data.vsphere_compute_cluster.cl.resource_pool_id
}

resource "vsphere_virtual_machine" "terraform-debian11-vm" {
	name         	 = "terraform-debian11-vm"
	datastore_id     = "${data.vsphere_datastore.storage.id}"
	resource_pool_id = "${data.vsphere_compute_cluster.cl.resource_pool_id}"
	num_cpus     	 = 2
	memory       	 = 4096
	guest_id     	 = "${data.vsphere_virtual_machine.debian11.guest_id}"

	wait_for_guest_net_timeout = 0
	wait_for_guest_net_routable = false
	wait_for_guest_ip_timeout = 0

	network_interface {
	            network_id = "${data.vsphere_network.dpg-test-network.id}"
			  }
		

	network_interface {
	            network_id = "${data.vsphere_network.dpg1.id}"
		          }

	network_interface {
	            network_id = "${data.vsphere_network.dpg2.id}"
		          }

	disk {
	      label = "disk0"
	      size  = 100
	      thin_provisioned = false
	     }

	clone {
		template_uuid = "${data.vsphere_virtual_machine.debian11.id}"
	        linked_clone  = "false"

		customize {
			    linux_options {
			    		    host_name = "terraform-debian11-vm"
			    		    domain = ""
					  }
           	            network_interface {
					        ipv4_address = "10.1.0.10"
						ipv4_netmask = 24
					      }

		             network_interface {
						ipv4_address = "10.2.0.20"
						ipv4_netmask = 23
				      		}

		             network_interface  {
				  		 ipv4_address = "10.3.0.30"
						 ipv4_netmask = 24
				      		}
		   	     ipv4_gateway = "10.3.0.1"
		 	   }
	      }

	extra_config = {
		      "guestinfo.metadata" 	    = base64encode(file("meta-data"))
		      "guestinfo.metadata.encoding" = "base64"
		      "guestinfo.userdata"	    = base64encode(file("user-data"))
		      "guestinfo.userdata.encoding" = "base64"
		     }
}
